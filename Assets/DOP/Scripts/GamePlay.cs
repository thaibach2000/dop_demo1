﻿using UnityEngine;
using UnityEngine.UI;


public class GamePlay : MonoBehaviour
{
    public Transform LevelParent;
    //public static Action<int, float, object[]> Test;

    public static GamePlay Ins;
    int levelCurrent = 0;
    public GameObject[] levels;

    public GameObject oldObj, newObj;

    [SerializeField] LineRenderer line;
    [SerializeField] Text levelCount;
    [SerializeField] Toggle audiotogle;

    void Start()
    {
        GenerateObject(0);
        if (AudioListener.volume == 0)
        {
            audiotogle.isOn = false;
        }
        GameObject.FindWithTag("Mark").GetComponent<SpriteRenderer>().enabled = false;

    }
    void GenerateObject(int _index)
    {
        GameObject obj = Instantiate(levels[_index]) as GameObject;
        obj.transform.SetParent(LevelParent);
        obj.GetComponent<RectTransform>().localScale = Vector3.one;
        //GameObject.FindWithTag("Mark").SetActive(false);
      // chịu khó học các thuật toán căn bản với luyện code các logic cơ bản trước đi rồi bắt đầu vào làm những dạng thực tế kiểu này sẽ dễ hơn
            
        if (newObj != null)
        {
            oldObj = newObj;
        }
        newObj = obj;
        if (oldObj != null)
        {
            Destroy(oldObj);
            
        }
        levelCurrent += 1;
        Debug.Log("level : " + levelCurrent);
        levelCount.text = "" + levelCurrent;
    }

    public void NextGenerate()
    {
        if (levelCurrent >= (levels.Length))
        {
            return;
        }
        GenerateObject(levelCurrent);
    }
    void Awake()
    {
        Ins = this;
    }
    void Update()
    {
    }

}





