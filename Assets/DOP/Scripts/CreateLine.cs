﻿using UnityEngine;
using UnityEngine.UI;


public class CreateLine : MonoBehaviour
{

    //public static Action<int, float, object[]> Test;
    public Transform LevelParent;
    [SerializeField] public static CreateLine Ins;
    int vertexCount = 0;

    bool mouseDown = false;

    bool check = true;
    int CountPoint = 0;
    bool p1 = false, p2 = false, p3 = false, p4 = false;
    //GameObject game;
    [SerializeField] LineRenderer line;

    void Start()
    {

    }
    void Awake()
    {
        Ins = this;
        line = GetComponent<LineRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        int layerMask = LayerMask.GetMask(new string[] { "Default", "Water", "UI" });
        #region chạy trên android
        //if (Application.platform == RuntimePlatform.Android)
        //{
        //    if (Input.touchCount > 0)
        //    {
        //        // Gets only the first touch (No multi-touch)
        //        if (Input.GetTouch(0).phase == TouchPhase.Moved)
        //        {
        //            line.positionCount = vertexCount + 1;
        //            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //            line.SetPosition(vertexCount, mousePos);
        //            vertexCount++;
        //            //đặt điều kiện dùng raycastAll
        //            RaycastHit2D[] hit = Physics2D.RaycastAll(mousePos, Vector2.zero, layerMask);
        //            for (int i = 1; i <= hit.Length; i++)// nếu i = 0 thì null và ko check đc null, nếu để i = 1 thì trong vùng mặc định !=null
        //            {
        //                // nếu trong này chạm tất cả các điểm kể cả collider polygon ->break
        //                //check chỉ đc ở trong ko đc cả 2   
        //                if (hit[i].collider != null)
        //                {
        //                    Destroy(gameObject, 0.3f);
        //                    GameObject.FindWithTag("Mark").SetActive(true);
        //                    Debug.Log("trong");
        //                    Destroy(gameObject);// xóa line luôn
        //                                        // thêm effect nữa là ok
        //                }
        //                if (hit[i].collider == null)
        //                {
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //}
        //// Windows specific code (Editor)
        #endregion
        //else
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                mouseDown = true;
            }
            if (mouseDown)
            {
                //tạo đường vẽ
                line.positionCount = vertexCount + 1;
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                line.SetPosition(vertexCount, mousePos);
                vertexCount++;

                RaycastHit2D[] hit = Physics2D.RaycastAll(mousePos, Vector2.zero, layerMask);
                for (int i = 0; i < hit.Length; i++)//chạy các tia của raycast
                {
                    if(hit[i].collider == null)
                    {
                        check = false;
                    }
                    else
                    {
                        if(hit[i].collider.tag == "P1")
                        {
                            p1 = true;
                            Debug.Log("hit");
                        }else if(hit[i].collider.tag == "P2")
                        {
                            p2 = true;
                        }
                        else if (hit[i].collider.tag == "P3")
                        {
                            p3 = true;
                        }
                        else if (hit[i].collider.tag == "P4")
                        {
                            p4 = true;
                        }
                    }

                }
                
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("!");
            mouseDown = false;
            if (p1 && p2 && p3 && p4 && check)
            {
                Debug.Log("work!");
                GameObject.FindWithTag("Mark").GetComponent<SpriteRenderer>().enabled = true;//hic hic
            }
            else
            {
                check = true;
                p1 = false;
                p2 = false;
                p3 = false;
                p4 = false;
            }
            line.positionCount = 0;
            vertexCount = 0;
        }
    }
}
